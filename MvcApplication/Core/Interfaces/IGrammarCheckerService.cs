﻿using Core.Models;

namespace Core.Interfaces
{
    public interface IGrammarCheckerService
    {
        OutputModel Check(string text);
    }
}