﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Core.Helpers
{
    public static class Helpers
    {
        public static string FormatOutput(string input, GrammarError grammarError)
        {
            var errorStart = grammarError.ErrorNumbers[0];
            var amountOfCharactersToHighlight = grammarError.ErrorNumbers[1];

            var spanLength = "<span>".Length;

            var textWithAddedTag = input.Insert(errorStart, "<span>");
            var textWithSpan = textWithAddedTag.Insert(errorStart + amountOfCharactersToHighlight + spanLength, "</span>");

            return textWithSpan;
        }
    }
}
