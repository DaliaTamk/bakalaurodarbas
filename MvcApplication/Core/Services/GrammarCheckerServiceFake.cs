﻿using System;
using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;

namespace Core.Services
{
    public class GrammarCheckerServiceFake : IGrammarCheckerService
    {
        public OutputModel Check(string text)
        {
            var grammarErrorList = new List<GrammarError>
                                   {
                                       new GrammarError
                                       {
                                           ErrorType =
                                               "splitting_non_abbreviating_forms",
                                           Suggestions = new List<string>
                                           {
                                               "ką tik",
                                               "tekstas"
                                           },
                                           ErrorNumbers = new List<int> { 6, 5 }
                                       },
                                       new GrammarError
                                       {
                                           ErrorType = "LTFOT_PHRASES",
                                           Suggestions = new List<string>
                                           {
                                               "ką tik"
                                           },
                                           ErrorNumbers = new List<int> { 6, 5 }
                                       },
                                       new GrammarError
                                       {
                                           ErrorType = "ualgiman_kablelis_pries_o_bet_taciau_tik_nors",
                                           Suggestions = new List<string>()
                                           {
                                               "čigonus, bet"
                                           },
                                           ErrorNumbers = new List<int> { 21, 11 }
                                       },
                                       new GrammarError
                                       {
                                           ErrorType = "SENTENCE_WHITESPACE",
                                           Suggestions = new List<string>()
                                           {
                                               " Kas"
                                           },
                                           ErrorNumbers = new List<int> { 41, 3 }
                                       }
                                   };

            text = "Jonas kątik buvo pas čigonus bet neilgam.Kas toliau nutiko niekas nežino.";


            for (var index = 0; index < grammarErrorList.Count; index++)
            {
                grammarErrorList[index].Output = Helpers.Helpers.FormatOutput(text, grammarErrorList[index]);
            }

            var result = new OutputModel(grammarErrorList);

            return result;
        }
    }
}