﻿using System.Collections;
using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Core.Services
{
    public class SpellCheckerService
    {
        public string Check(string text)
        {
            var lexClient = new RestClient("http://nagys.vdu.lt:7000/lex");
            var lexRequest = new RestRequest(Method.POST) { RequestFormat = DataFormat.Json };
            lexRequest.AddParameter("text/plain", text,ParameterType.RequestBody);
            var response = lexClient.Execute(lexRequest);
            var lexResponse = lexClient.Execute<Lex>(lexRequest);

            var annotations = new { lex = lexResponse.Data};
            var messageForSpellingService = new { body = text, annotations = annotations };
            var spellingServiceClient = new RestClient("http://vienuolis.vdu.lt:8081/spelling");
            var spellingServiceRequest = new RestRequest(Method.POST) { RequestFormat = DataFormat.Json };
            spellingServiceRequest.AddBody(messageForSpellingService);
            var spellingResponse = spellingServiceClient.Execute(spellingServiceRequest);
        
            return spellingResponse.Content;
        }
    }
}
