﻿using System.Collections;
using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Core.Services
{
    public class GrammarCheckerService : IGrammarCheckerService
    {
        public OutputModel Check(string text)
        {
            var message = new {body = text};

            var client = new RestClient("http://vienuolis.vdu.lt:8084/grammar");
            var request = new RestRequest(Method.POST) { RequestFormat = DataFormat.Json };
            request.AddBody(message);
            var response = client.Execute(request);
            var outputModel = JsonConvert.DeserializeObject<OutputModel>(response.Content);
            if (outputModel.GrammarErrors != null)
            {
                for (var index = 0; index < outputModel.GrammarErrors.Count; index++)
                {
                    outputModel.GrammarErrors[index].Output = Helpers.Helpers.FormatOutput(text, outputModel.GrammarErrors[index]);
                }

                return outputModel;
            }

            return null;
        }
    }
}