﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Lex
    {
        public List<List<int>> seg { get; set; }
        public List<List<int>> s { get; set; }
        public List<List<int>> p { get; set; }
    }
}
