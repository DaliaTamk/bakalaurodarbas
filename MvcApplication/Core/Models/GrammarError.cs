﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Core.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class GrammarError
    {
        [JsonProperty(PropertyName = "ref")]
        public List<int> ErrorNumbers;
        [JsonProperty(PropertyName = "error_type")]
        public string ErrorType;
        [JsonProperty(PropertyName = "suggestions")]
        public List<string> Suggestions;

        public string Output;
    }
}