﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public class OutputModel
    {
        [JsonProperty(PropertyName = "grammar_errors")]
        public List<GrammarError> GrammarErrors { get; set; }

        public OutputModel()
        {
        }

        public OutputModel(List<GrammarError> grammarErrors)
        {
            GrammarErrors = grammarErrors;
        }
    }
}
