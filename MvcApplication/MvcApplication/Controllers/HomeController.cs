﻿using System.IO;
using System.Text;
using System.Web.Mvc;
using Core.Interfaces;
using Core.Services;

namespace MvcApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGrammarCheckerService service = new GrammarCheckerService();
        private readonly SpellCheckerService spellService = new SpellCheckerService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Spell()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UseGrammarCheckerService(string text)
        {
            var result = service.Check(text);
            var sb = new StringBuilder();

            if (result == null)
            {
                sb.Append(RenderRazorViewToString("_SuccessResult", "Gramatinių klaidų nerasta"));
                return Json(new { Html = sb.ToString() });
            }

            foreach (var error in result.GrammarErrors)
            {
                sb.Append(RenderRazorViewToString("DisplayErrors", error));
            }

            return Json(new { Html = sb.ToString()});
        } 

        public JsonResult UseSpellCheckerService(string text)
        {
            var result = spellService.Check(text);
            return Json(new { Html = result});
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

    }
}
